# bookshelf

Aplikasi backend sederhana yang menyediakan API untuk menambah, mengubah, menampilkan dan menghapus data buku (mengelola data buku). Data disimpan menggunakan sqlite3.

## Technology

- Go 1.18.1
- Sqlite3

## Getting Started

``` go
//  Clone this repo to your local machine using
git clone https://gitlab.com/hilmanfitriana19/bookshelf.git

//  Get into the directory
cd test-rest

//  Install dependencies
go mod download

//  Run App
go run example.com/test-rest

```

## API Endpoint

|API|Routes|Method|
|----------------|-------------------------------|-----------------------------|
|Register Book |/books|POST|
|Update Book |/api/books/:id|PATCH|
|Delete Book |/api/books/:id|DELETE|
|Get All Book |/api/books|GET|
|Get Book |/api/books/:id|GET|


## Source Awal
[How to build a REST API with Golang using Gin and Gorm](
https://blog.logrocket.com/how-to-build-a-rest-api-with-golang-using-gin-and-gorm/)