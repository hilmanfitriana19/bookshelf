package main

import (
	"github.com/gin-gonic/gin"

	"example.com/test-rest/models"

	"example.com/test-rest/controllers"
)

func main() {
	r := gin.Default()

	models.ConnectDatabase()

	// list all available endpoint dengan prosesnya
	r.GET("/books", controllers.GetAllBooks)
	r.POST("/books", controllers.CreateBooks)
	r.GET("/books/:id", controllers.GetBook)
	r.PUT("/books/:id", controllers.UpdateBook)
	r.DELETE("/books/:id", controllers.DeleteBook)

	// run dengan port yang didefinisikan
	r.Run(":8082")
}
