package controllers

// modul controller terkait data book

import (
	"net/http"

	"example.com/test-rest/models"

	"github.com/gin-gonic/gin"
)

// struct untuk validasi input data buku
type CreateBookInput struct {
	Title  string `json:"title" binding:"required"`
	Author string `json:"author" binding:"required"`
}

// struct untuk validasi update data buku
type UpdateBookInput struct {
	Title  string `json:"title"`
	Author string `json:"author"`
}

// handler untuk api get data seluruh buku
func GetAllBooks(c *gin.Context) {

	var books []models.Book //create slice
	models.DB.Find(&books)  //query to db
	c.JSON(http.StatusOK, gin.H{"data": books})
}

// handler untuk membuat data book baru
func CreateBooks(c *gin.Context) {

	var input CreateBookInput

	if err := c.ShouldBindJSON(&input); err != nil { //validate input
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Save to DB
	book := models.Book{Title: input.Title, Author: input.Author} //create data buku
	models.DB.Create(&book)                                       //save to db

	c.JSON(http.StatusOK, gin.H{"data": book})
}

// handler untuk mengambil data buku by id
func GetBook(c *gin.Context) {

	var book models.Book

	if err := models.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil { //query to db
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": book})
}

// handler untuk update data buku by id
func UpdateBook(c *gin.Context) {

	var book models.Book

	if err := models.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil { //query to db
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var input UpdateBookInput
	if err := c.ShouldBindJSON(&input); err != nil { //validate data
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	models.DB.Model(&book).Updates(input) //update data

	c.JSON(http.StatusOK, gin.H{"data": book})
}

// handler untuk menghapus data by id
func DeleteBook(c *gin.Context) {

	var book models.Book

	if err := models.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil { //query to db
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&book)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
